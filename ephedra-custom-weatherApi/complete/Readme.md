# Ephedra custom wrapper: weather API demo complete

This folder provide a complete app for the [weather api tutorial](../../tutorials/weather/weather.md).

## Deployment

1. Adjust the DarkSky API key in the _config/repositories/weather-api.ttl_ repository configuration
2. Follow the documentation on [app deployment](../../tutorials/deployment/deployment.md).

The basic example is available on http://localhost:10214/resource/?uri=http://www.example.org/WeatherDemo

In case the wikidata app is installed, the integration example for Portoroz can be found on http://localhost:10214/resource/wd:Q564988

