# Ephedra custom wrapper: linked data documents demo complete

This folder provide a complete app for the [linked data documents tutorial](../../tutorials/dblp/dblp.md).

## Deployment

1. Follow the documentation on [app deployment](../../tutorials/deployment/deployment.md).

The basic example is available on http://localhost:10214/resource/?uri=http://www.example.org/DblpDemo

In case the wikidata app is installed, the integration example for _Peter Haase_ can be found on http://localhost:10214/resource/?uri=http://www.example.org/DblpDemo

