# Ephedra custom wrapper: linked data documents demo source

This folder contains the source for the custom SAIL wrapper implementation. It
is an example of how custom services can be integrated through an ephedra federation.

## Build commands:

```
../../gradlew cleanEclipse eclipse
../../gradlew deploy
```