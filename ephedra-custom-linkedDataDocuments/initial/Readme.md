# Ephedra custom wrapper: linked data documents demo demo initial

This folder provide a initial base app for the [linked data documents tutorial](../../tutorials/dblp/dblp.md).

## Deployment

1. Follow the documentation on [app deployment](../../tutorials/deployment/deployment.md).


