# Example app for querying Linked Data Documents through Ephedra

This app demonstrates how a custom Linked Data Documents wrapper can be used with Ephedra to query Linked Data documents. As a use-case we query the geo-coordinates from the Wikidata knowledge graph, and display the three day forecast for the corresponding location.

A full tutorial describing the setup and scenario in detail is available [here](../tutorials/dblp/dblp.md).

The app is available in different stages of completion: _source_, _initial_ and _complete_.