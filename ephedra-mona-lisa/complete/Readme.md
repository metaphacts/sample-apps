# Ephedra custom wrapper: Mona Lisa complete

This folder provides a complete app for the [[mona lisa tutorial](../../tutorials/monalisa/monalisa.md)].

## Deployment

1. Follow the documentation on [app deployment](../../tutorials/deployment/deployment.md).

The basic example is available on http://localhost:10214/resource/?uri=http://www.example.org/MonaLisaDemo